$(document).ready(function() {
    $('.navigation-bar a').each(function() {
        if($(this).attr('href') == window.location.pathname || $(this).attr('href')+"/" == window.location.pathname) {
            $(this).css('color', '#31bde0');
        }
    });

    $('.overlay li a').each(function() {
        if($(this).attr('href') == window.location.pathname || $(this).attr('href')+"/" == window.location.pathname) {
            $(this).css('color', '#31bde0');
        }
    });

    $('.topnav .icon').click(function() {
        $(".overlay").css("visibility", "visible");
    });

    $('.overlay .icon').click(function() {
        $(".overlay").css("visibility", "hidden");
    });
});